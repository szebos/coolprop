mkdir build-conda
cd build-conda


REM cmake .. -DCOOLPROP_STATIC_LIBRARY:BOOL="1" -DCMAKE_BUILD_TYPE:STRING=Release -G"Ninja"
REM ninja install
REM cmake .. -DCOOLPROP_STATIC_LIBRARY:BOOL="1" -DCMAKE_BUILD_TYPE:STRING=Release -G"Ninja" -DCOOLPROP_LIBRARY_NAME:STRING="CoolPropd"
REM ninja install
REM xcopy %SRC_DIR%\install_root\static_library\Windows\64bit\*.lib %LIBRARY_LIB%
REM REM copy %SRC_DIR%\install_root\static_library\Windows\64bit_MSVC_19.24.28314.0\CoolProp.lib %LIBRARY_LIB%
REM REM copy %SRC_DIR%\install_root\%COOLPROP_STATIC_LIB_DIR%\CoolProp.lib %LIBRARY_LIB% 
REM xcopy %SRC_DIR%\include\*.h %LIBRARY_INC%\CoolProp\
REM xcopy %SRC_DIR%\externals\fmtlib\fmt\*.h %LIBRARY_INC%\CoolProp\fmt\*.h

cmake .. ^
-G"Visual Studio 15 2017 Win64" ^
-DCOOLPROP_STATIC_LIBRARY:BOOL="1" ^
-Wno-dev
if errorlevel 1 exit 1
MSBUILD /p:PlatformToolset=v141 /p:Configuration=Release /p:BuildInParallel=true /m:12 %SRC_DIR%\build-conda\ALL_BUILD.vcxproj
if errorlevel 1 exit 1
MSBUILD /p:PlatformToolset=v141 /p:Configuration=Release /p:BuildInParallel=true /m:12 %SRC_DIR%\build-conda\INSTALL.vcxproj
if errorlevel 1 exit 1
rem xcopy %SRC_DIR%\install_root\static_library\Windows\64bit\*.lib %LIBRARY_LIB%


cmake .. ^
-G"Visual Studio 15 2017 Win64" ^
-DCOOLPROP_STATIC_LIBRARY:BOOL="1" ^
-DCOOLPROP_LIBRARY_NAME:STRING="CoolProp" ^
-Wno-dev
if errorlevel 1 exit 1
REM  /p:DebugType=embedded = pass to cl /Z7
MSBUILD /p:PlatformToolset=v141 /p:Configuration=Debug /p:BuildInParallel=true /p:DebugType=embedded /m:12 %SRC_DIR%\build-conda\ALL_BUILD.vcxproj
if errorlevel 1 exit 1
MSBUILD /p:PlatformToolset=v141 /p:Configuration=Debug /p:BuildInParallel=true /p:DebugType=embedded /m:12 %SRC_DIR%\build-conda\INSTALL.vcxproj
if errorlevel 1 exit 1

xcopy %SRC_DIR%\install_root\static_library\Windows\64bit\*.lib %LIBRARY_LIB%
xcopy %SRC_DIR%\include\*.h %LIBRARY_INC%\CoolProp\
xcopy %SRC_DIR%\externals\fmtlib\fmt\*.h %LIBRARY_INC%\CoolProp\fmt\*.h